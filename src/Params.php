<?php

namespace Nikolajev\Params;

class Params
{
    final protected function getOrSet(array $definedVars): mixed
    {
        $paramTitle = array_keys($definedVars)[0];
        $paramValue = array_values($definedVars)[0];

        if ($paramValue === null) {
            return $this->$paramTitle;
        }

        $this->$paramTitle = $paramValue;
        return $this;
    }
}